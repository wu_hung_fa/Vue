const HtmlWebpackPlugin=require('html-webpack-plugin')
const htmlPlugin = new HtmlWebpackPlugin({
	template: './src/index.html',
	filename: 'index.html'
});
const path = require('path');
module.exports = {
	mode: 'development',
	entry: path.resolve(__dirname, './src/js/main.js'), //入口
	output: {
		path: path.resolve(__dirname, 'dist'),
		filename: 'bundle.js'
	},
	plugins:[htmlPlugin],
	module: {
	    rules: [
	      {
	        test: /\.css$/,
	        use: ['style-loader','css-loader']
	      },
		  {
		    test: /\.(png|jpg|gif|jpeg)$/,
		    use: [{
				loader:'url-loader',
				options:{
					limit:1000000
				},'file-loader'
			}]
		  }
	    ]
	  }
};
